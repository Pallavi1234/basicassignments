package basicAssignments;


import java.util.Scanner;

public class Factorial {

    public static void main(String[] args) {

        Integer number;
        System.out.println("Calculate factorial of a number");
        System.out.println("Enter a number : ");

        Scanner scan = new Scanner(System.in);
        number = scan.nextInt();

        Integer factorial = 1;

        for(Integer i = number; i>0; i--)
        {
            factorial = factorial * i;
        }

        System.out.println("Factorial of " + number + " is :" + factorial);
    }
}
