package basicAssignments;

import java.util.Scanner;

public class PrimeNumber {

    public static void main(String[] args) {

        System.out.println("Check if given number is prime :");
        Scanner scan = new Scanner(System.in);

        Integer number = scan.nextInt();

        if (number == 0 || number == 1) {
            System.out.println("Enter number other than 0 or 1");
        }
        else {
            Integer halfNum = number / 2;
            Boolean flag = false;

            for (Integer i = 2; i <= halfNum; i++) {
                if (number % i == 0) {
                    flag = true;
                    break;
                }
            }
            if (flag)
                System.out.println("Number is not prime");
            else
                System.out.println("Numer  is  prime");
        }

    }
}
