package basicAssignments;

public class MatrixOperations {

    public static void main(String[] args)
    {
        Integer[][] matrix1 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };


        Integer[][] matrix2 = {
                {1, 2, 3},
                {4, 5, 6},
                {7, 8, 9}
        };


        Integer[][] result = new Integer[3][3];

        for (int i=0; i<3; i++)
        {
            for (int j=0; j<3; j++)
            {
                result[i][j] = matrix1[i][j] + matrix2[i][j];
            }
        }

        System.out.println("\n Matrix Addition ");
        for (Integer[] arr: result) {
            for (Integer a: arr) {
                System.out.print(" " + a);
            }
            System.out.println();
        }


        System.out.println("\n Matrix Multiplication ");

        for(int i=0;i<3;i++){
            for(int j=0;j<3;j++){
                result[i][j]=0;
                for(int k=0;k<3;k++)
                {
                    result[i][j]+=matrix1[i][k]*matrix2[k][j];
                }
                System.out.print(result[i][j]+" ");
            }
            System.out.println();
        }
    }
}
